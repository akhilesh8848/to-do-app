import 'package:flutter/material.dart';
import 'package:todo_app/ToDoApp.dart';
import 'package:firebase_core/firebase_core.dart';

class MyApp extends StatefulWidget {
  @override
  _MyAppState createState() => _MyAppState();
}



class _MyAppState extends State<MyApp> {

  bool _initialized=false;
  bool _error=false;

  void initializeFlutterFire() async {
    try {
      await Firebase.initializeApp();
      setState(() {
        _initialized = true;
      });
    }
    catch (e) {
      setState(() {
        _error = true;
      });
    }
  }

  void initState() {
    initializeFlutterFire();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'ToDo',
      debugShowCheckedModeBanner: false,
      theme: ThemeData.dark().copyWith(
        accentColor: Colors.yellowAccent
      ),
      home: ToDoApp(),
    );
  }
}

void main() => runApp(MyApp());

