import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:cloud_firestore/cloud_firestore.dart';


class ToDoApp extends StatefulWidget {
  @override
  _ToDoAppState createState() => _ToDoAppState();
}

class _ToDoAppState extends State<ToDoApp> {
  String text;


  createToDo(){
    DocumentReference documentReference=FirebaseFirestore.instance.collection('Todos').doc(text);

    Map<String,String> todos={
      "todoTitle":text
    };

    documentReference.set(todos).whenComplete(() => {print("$text created")});
  }

  deleteToDo(item){
    DocumentReference documentReference=FirebaseFirestore.instance.collection('Todos').doc(item);
    documentReference.delete().whenComplete(() => {print("deleted")});
  }

  @override

  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('ToDo App',
          textAlign: TextAlign.center,
          style: TextStyle(
              fontWeight: FontWeight.bold
          ),
        ),
        backgroundColor: Colors.black,
        centerTitle: true,
      ),
      body:StreamBuilder(stream: FirebaseFirestore.instance.collection("Todos").snapshots(),builder:(context,snapshots){
        return ListView.builder
          (
            shrinkWrap: true,
            itemCount: snapshots.data.docs.length,
            itemBuilder: (context, index) {
              DocumentSnapshot documentSnapshot=snapshots.data.docs[index];
              return Dismissible(
                onDismissed: (direction){
                  deleteToDo(documentSnapshot['todoTitle']);
                },
                key: Key(index.toString()),
                child: Card(
                  shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(10)
                  ),
                  child: ListTile(
                    title: Text(
                        documentSnapshot['todoTitle']
                    ),
                    trailing: IconButton(
                      icon: Icon(Icons.delete,color: Colors.red,),
                      onPressed: (){
                        deleteToDo(documentSnapshot['todoTitle']);
                      },
                    ),
                  ),
                ),
              );
            }
        );
      },),
      backgroundColor: Colors.black,
      floatingActionButton: FloatingActionButton(
        onPressed: (){
            showDialog(context: context, builder: (BuildContext context){
              return AlertDialog(
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(5)
                ),
                title: Text('Add ToDo',),
                content: TextField(
                  onChanged: (value){
                    text=value;
                  },
                  autofocus: true,
                ),
                actions: <Widget>[
                  RaisedButton(
                    onPressed: (){
                      createToDo();
                      Navigator.of(context).pop();
                    },
                    child: Text('Add'),
                    color: Colors.yellowAccent,
                  ),
                  RaisedButton(

                    onPressed: (){
                      Navigator.of(context).pop();
                    },
                    child: Text('Cancel'),
                    color: Colors.yellowAccent,
                  )
                ],
              );
            });
        },
        backgroundColor: Colors.yellowAccent,
        child: Icon(Icons.add,color: Colors.black,),
      ),
    );
  }
}
